# Automated Resume

R provide us a great set of tools useful for daily tasks automation.
One of these tools is [pagedown](https://github.com/rstudio/pagedown).

Following its [manual](https://pagedown.rbind.io/), using the power
and simplicity of Markdown, the dynamism of R, and making some CSS
customizations, it can be easy to create a maintainable resume template.

This project is the result of many years searching for a tool that allows
to update the information is a more efficient way. No more struggling with
document editors or recalculating dates!

Now updating the resume is easy and fun! At least for developers ;)
